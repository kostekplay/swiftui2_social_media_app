////  SwiftUI2_SocialMediaAppApp.swift
//  SwiftUI2_SocialMediaApp
//
//  Created on 11/03/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_SocialMediaAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
